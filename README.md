# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| None                                             | 1~5%      | N         |
### URL
**https://107062334.gitlab.io/As_01_WebCanvas/**

### How to use (**Need internet to load some icons.**)
![](https://i.imgur.com/5TFe4D1.png) ![](https://i.imgur.com/4doWYHf.png) Click the pen icon to start drawing on the canvas and click the eraser icon to remove what is drawn on the canvas.
The cursor icon will change to pen icon and eraser icon when corresponding button is clicked and the mouse is in the canvas.

![](https://i.imgur.com/SL1Nkq2.png) ![](https://i.imgur.com/wv3QHGA.png) You can press undo and redo to recover to last step.

![](https://i.imgur.com/yclB7n6.png) Reset will set everything to its default status, including typeface, fontsize, cursor icon, line width, canvas, and color, and remove textbox and everything drawn on the canvas.
Reset will also empty undo and redo, which means that you cannot undo and redo after reset.

![](https://i.imgur.com/fXNTg7W.png) Text button will create a textbox, which can be placed on the canvas.
When there is a textbox on the canvas, press enter will draw the text on the canvas and remove the textbox.

![](https://i.imgur.com/p4FgSNG.png) ![](https://i.imgur.com/O04LBSS.png) ![](https://i.imgur.com/ftcYt3c.png) Click these icon to draw a triangle or a circle or a rectangle.
The cursor icon will also change.

![](https://i.imgur.com/SgZ9e0P.png) Click this icon to select fill or stroke when you draw polygons.


![](https://i.imgur.com/SKJJsDf.png) ![](https://i.imgur.com/IbhFg7L.png) These two can upload an image or download the image from canvas.

![](https://i.imgur.com/dOP97P4.png) ![](https://i.imgur.com/oCb9D7q.png) Use them to change the line color and line width.


![](https://i.imgur.com/NcftsY3.png) ![](https://i.imgur.com/COmQNj7.png) Use these two can choose typeface and fontsize of text.


### Function description

#### Pen ![](https://i.imgur.com/5TFe4D1.png) and Eraser ![](https://i.imgur.com/4doWYHf.png)

When pen is clicked, it will call "penonclick()" to execute the onclick function.

```
<button type="button" id="pen" onclick=penonclick() class="sbtn btn btn-primary"><img src="icon/pen_cursor.png"></button>
```

In penonclick, it will set the cursor icon to the pen icon and set the mode to "pen".
If there is any textbox that is in the canvas, it will remove that.

```
function penonclick()
{   
    ctx.globalCompositeOperation = "source-over";
    $("#canvas").css("cursor","url('./icon/pen_cursor.png'),auto");
    mode="pen";
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    
}
```

After calling penonclick, it will call "draw" when mouse is clicked and moving.
While moving, mouse coordinate will keep updating until mouse is released.

```
$("#canvas").mousemove(update_mousecoord);
$("#canvas").mousemove(draw);

function update_mousecoord(event)
{   
    if (clicked == true)
    {
        mousecoord.pre_x = mousecoord.cur_x;
        mousecoord.pre_y = mousecoord.cur_y;
        mousecoord.cur_x = event.pageX
        mousecoord.cur_y = event.pageY;
    } 
}

function draw(event)
{   
    if (clicked == true && mode == "pen")
    {   
        ctx.beginPath();
        ctx.moveTo(mousecoord.pre_x-12,mousecoord.pre_y+10);
        ctx.lineTo(mousecoord.cur_x-12,mousecoord.cur_y+10);
        ctx.stroke();
    }
    else if (clicked == true && mode == "eraser")
    {   
        ctx.globalCompositeOperation = "destination-out";
        ctx.beginPath();
        ctx.moveTo(mousecoord.pre_x,mousecoord.pre_y);
        ctx.lineTo(mousecoord.cur_x,mousecoord.cur_y);
        ctx.stroke();
        ctx.globalCompositeOperation = "source-over";
    }
    else if (clicked == true && mode == "triangle")
    {   
        ctx.clearRect(0,0,1000,700);
        var length_x = origin_x - mousecoord.cur_x;
        var length_y = origin_y - mousecoord.cur_y;
        ctx.beginPath();
        ctx.moveTo(origin_x,origin_y);
        ctx.lineTo(origin_x-length_x,origin_y-length_y);
        ctx.lineTo(origin_x+length_x,origin_y-length_y);
        ctx.closePath();
        ctx.putImageData(img,0,0);
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
    else if (clicked == true && mode == "circle")
    {
        ctx.clearRect(0,0,1000,700);
        var radius = Math.sqrt((origin_x-mousecoord.cur_x)*(origin_x-mousecoord.cur_x)+(origin_y-mousecoord.cur_y)*(origin_y-mousecoord.cur_y))/2;
        ctx.beginPath();
        ctx.arc((origin_x+mousecoord.cur_x)/2,(origin_y+mousecoord.cur_y)/2,radius,0,2*Math.PI);
        ctx.putImageData(img,0,0);
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
    else if (clicked == true && mode == "rectangle")
    {
        ctx.clearRect(0,0,1000,700);
        var length_x = mousecoord.cur_x - origin_x;
        var length_y = mousecoord.cur_y - origin_y;
        ctx.beginPath();
        ctx.moveTo(origin_x,origin_y);
        ctx.lineTo(origin_x+length_x,origin_y);
        ctx.lineTo(origin_x+length_x,origin_y+length_y);
        ctx.lineTo(origin_x,origin_y+length_y);
        ctx.closePath();
        ctx.putImageData(img,0,0);
        
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
}
```

If mouse is clicked or released, it will update the variable "clicked" to true or false in order to represent whether the mouse is being clicked or not.
It will also record the current canvas image, which helps to undo the canvas.

```
$(window).mousedown(update_state_down);
$(window).mouseup(update_state_up);
$("#canvas").mousedown(update_imagedata);

function update_imagedata(event)
{
    if (clicked == false && event.button == 0 && mode!="text")
    {   
        redo_list=[];
        var imga = ctx.getImageData(0,0,1000,700);
        undo_list.push(imga);
        
    }
}

function update_state_down(event)
{
    if (clicked == false && event.button == 0)
    {   
        if (mode == "triangle" || mode == "circle" || mode == "rectangle")
        {
            img = ctx.getImageData(0,0,1000,700);
        }
        clicked = true;
        mousecoord.cur_x = event.pageX;
        mousecoord.cur_y = event.pageY;
        origin_x = mousecoord.cur_x;
        origin_y = mousecoord.cur_y;
    }
}

function update_state_up(event)
{
    if (clicked == true && event.button == 0)
    {  
        clicked = false;
    }
}
```

Eraser is similar to pen.
The only difference is that when drawing, "ctx.globalCompositeOperation" must be set to "destination-out" to erase the area mouse go through.
```
else if (clicked == true && mode == "eraser")
{   
    ctx.globalCompositeOperation = "destination-out";
    ctx.beginPath();
    ctx.moveTo(mousecoord.pre_x,mousecoord.pre_y);
    ctx.lineTo(mousecoord.cur_x,mousecoord.cur_y);
    ctx.stroke();
    ctx.globalCompositeOperation = "source-over";
}
```

#### Undo ![](https://i.imgur.com/SL1Nkq2.png) and Redo ![](https://i.imgur.com/wv3QHGA.png)

Undo's and redo's onclick function are "undo()" and "redo()" respectively.
```
<button id="undo" onclick=undo() type="button" class="sbtn btn btn-danger"><img src="https://img.icons8.com/ios-glyphs/30/000000/undo.png"/></button>
<button id="redo" onclick=redo() type="button" class="sbtn btn btn-warning"><img src="https://img.icons8.com/ios-glyphs/30/000000/redo.png"/></button>
```

In undo, if there are still some images in undo_list, it will get the latest canvas image and push the current canvas to redo_list and then draw.

```
function undo()
{   
    if (undo_list.length!=0)
    {   
        var cur_img = ctx.getImageData(0,0,1000,700);
        redo_list.push(cur_img);
        var prev_img = undo_list.pop();
        ctx.putImageData(prev_img,0,0);
    }  
}
```

Redo function is similar to undo but it is in reverse.

```
function redo()
{   
    if (redo_list.length!=0)
    {   
        var cur_img = ctx.getImageData(0,0,1000,700);
        undo_list.push(cur_img);
        var next_img = redo_list.pop(); 
        ctx.putImageData(next_img,0,0);
    }    
}
```
#### Text ![](https://i.imgur.com/fXNTg7W.png)
Text's onclick function is "textonclick()".
```
<button id="text" onclick=textonclick() type="button" class="sbtn btn btn-primary"><img src="https://img.icons8.com/android/24/000000/text-color.png"/></button>
```
"textonclick()" will only set the cursor icon to the text icon and set the mode to "text".
```
function textonclick()
{   
    $(canvas).css("cursor","url('https://img.icons8.com/android/24/000000/text-color.png'),auto");
    mode = "text";
}
```
Main function is call when the mouse is clicked in canvas.
"textbox" will set "typing_text" to true, which means that there is a textbox in the canvas.
Then, it creates a html "input" element with type "text".
Its "position" is set to absolute and "z-index" is set to 1, which is higher than canvas'.
Using margin to set the coordinate of textbox.
```
$("#canvas").mousedown(textbox);

function textbox(event)
{   
    if (typing_text == false && mode == "text")
    {
        typing_text = true;
        var text_input = document.createElement("input");
        text_input.id="text_input";
        var r = document.getElementById("left");
        var b = document.getElementById("body");
        text_input.setAttribute("type","text");
        text_input.setAttribute("autofocus","true");
        $(text_input).css("position","absolute");
        $(text_input).css("z-index","1");
        $(text_input).css("margin-top",event.clientY-19);
        $(text_input).css("margin-left",event.clientX-13);
        b.insertBefore(text_input,r);
    }    
}
```
Pressing enter allows you to print the text in the textbox to the canvas.
Before printing the text, it will save the current canvas to undo_list and remove the html element of textbox.
```
$(document).keypress(keyboardfunction);

function keyboardfunction(event)
{
    if (event.key == "Enter" && mode == "text" && typing_text == true)
    {   
        var imgb = ctx.getImageData(0,0,1000,700);
        undo_list.push(imgb);
        redo_list=[];
        var str = $("#text_input").val();
        var coordx = parseInt($("#text_input").css("margin-left"));
        var coordy = parseInt($("#text_input").css("margin-top"));
        $("#text_input").remove();
        typing_text = false;
        ctx.font = fontsize + "px " + typeface;
        ctx.fillText(str,coordx,coordy+11);
        
    }
}
```

#### Reset ![](https://i.imgur.com/yclB7n6.png)

Reset's onclick function is "reset()".
```
<button id="reset" onclick=reset() type="button" class="sbtn btn btn-success"><img src="https://img.icons8.com/ios-glyphs/30/000000/delete-sign.png"/></button>
```
Reset function will reset everything to its default status and clean the canvas.
If there is any textbox, it will be removed.
```
function reset()
{   

    mode = "none";
    clicked = false;
    typing_text = false;
    undo_list=[];
    redo_list=[];
    var t = document.getElementById("text_input");
    if (t) $("#text_input").remove();
    var color = document.getElementById("color");
    color.value="black";
    ctx.fillStyle = color.value;
    ctx.strokeStyle = color.value;
    ctx.clearRect(0,0,1000,700);
    $("#canvas").css("cursor","auto");
    var pensize = document.getElementById("pensize");
    pensize.value=10;
}
```

#### Triangle ![](https://i.imgur.com/p4FgSNG.png) Circle ![](https://i.imgur.com/O04LBSS.png) Rectangle ![](https://i.imgur.com/ftcYt3c.png) 
Triangle's onclick function is "triangleonclick()".
Circle's onclick function is "circleonclick()".
Rectangle's onclick function is "Rectangleonclick()".
```
<button id="triangle" onclick=triangleonclick() type="button" class="sbtn btn btn-danger"><img src="https://img.icons8.com/android/24/000000/triangle-stroked.png"/></button>
<button id="circle" onclick=circleonclick() type="button" class="sbtn btn btn-warning"><img src="https://img.icons8.com/material-rounded/24/000000/circled.png"/></button>
<button id="rectangle" onclick=rectangleonclick() type="button" class="sbtn btn btn-info"><img src="https://img.icons8.com/android/24/000000/rectangle-stroked.png"/></button>
```
Their onclick function basically do the same thing.
The differences are mode, cursor icon and "fill or stroke" icon.
```
function triangleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('https://img.icons8.com/android/24/000000/triangle-stroked.png'),auto")
    mode = "triangle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/material-two-tone/24/000000/triangle-stroked.png");
    else $("#fillorstrokeimg").attr("src","https://img.icons8.com/android/24/000000/triangle-stroked.png"); 
}

function circleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('https://img.icons8.com/material-sharp/24/000000/circled.png'),auto")
    mode = "circle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-glyphs/32/000000/filled-circle.png");
    else $("#fillorstrokeimg").attr("src","https://img.icons8.com/material-outlined/32/000000/filled-circle--v2.png");
}

function rectangleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('https://img.icons8.com/android/24/000000/rectangle-stroked.png'),auto")
    mode = "rectangle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-filled/24/000000/rectangle-stroked.png");
    else $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-glyphs/24/000000/rectangle-stroked.png");
}
```

#### Fill or Stroke ![](https://i.imgur.com/SgZ9e0P.png)
"fillorstroke" is the onclick function.
```
<button id="fillorstroke" onclick=fillorstroke() type="button" class="sbtn btn btn-secondary"><img id="fillorstrokeimg" src="https://img.icons8.com/android/24/000000/triangle-stroked.png"></button>
```

When clicked, it changes the "fors" variable to "fill" or "stroke".
The icon changes according to current status of variable "mode" and "fors".

```
function fillorstroke()
{
    fors = (fors == "stroke")?"fill":"stroke";
    if (mode == "triangle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/material-two-tone/24/000000/triangle-stroked.png");
        else $("#fillorstrokeimg").attr("src","https://img.icons8.com/android/24/000000/triangle-stroked.png"); 
    }
    else if (mode == "circle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-glyphs/32/000000/filled-circle.png");
        else $("#fillorstrokeimg").attr("src","https://img.icons8.com/material-outlined/32/000000/filled-circle--v2.png");
    }
    else if (mode == "rectangle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-filled/24/000000/rectangle-stroked.png");
        else $("#fillorstrokeimg").attr("src","https://img.icons8.com/ios-glyphs/24/000000/rectangle-stroked.png");
    }
}
```

#### Upload image ![](https://i.imgur.com/SKJJsDf.png) and download image ![](https://i.imgur.com/IbhFg7L.png)
Download's onclick function only needs to set the href to the canvas' data URL.
```
<a id="download" href="#canvas" download="canvas.jpg" onclick="this.href=document.getElementById('canvas').toDataURL()" class="btn btn-link" style="background-color: #007bff; border-color: #007bff; margin:38.74px;">
<img src="https://img.icons8.com/metro/26/000000/download.png"/></a>
```
Upload's onclick function is "imageonchange()".
It only accepts image files.
```
<input type="file" id="image_input" accept="image/*" onchange=imageonchange(event) class="btn btn-primary" style="margin:38.74px;">
```
In imageonchange, it will first create a image object and then assign it to the input image.
After saving current canvas, the input image is drawn.
```
function imageonchange(event)
{   
    var i = document.getElementById("image_input");
    var imgc = new Image();
    imgc.src = window.URL.createObjectURL(i.files[0]);
    var imgi = ctx.getImageData(0,0,1000,700);
    undo_list.push(imgi);
    redo_list=[];
    imgc.addEventListener("load",function()
    {   
        i.value = "";
        ctx.drawImage(imgc,0,0);
    })
}
```

#### Color picker ![](https://i.imgur.com/dOP97P4.png) and Pen size ![](https://i.imgur.com/oCb9D7q.png)

Color picker and Pen size are html built-in elements.
```
<input id="color" type=color style="width:56px;height:25px;margin:45.23px;">
<input id="pensize" class="slider" type=range min=8 max=96 value=10 style="margin:45.23px;">
```
When the value changes, it will call the callback function to change corresponding variables.
```
$("#color").change(function(){ctx.fillStyle=this.value;ctx.strokeStyle=this.value;});
$("#pensize").change(function(){ctx.lineWidth=this.value;});
```
#### Typeface ![](https://i.imgur.com/NcftsY3.png) and Fontsize ![](https://i.imgur.com/COmQNj7.png)
The "Typeface" and "Fontsize" is button type, and the options in the menu is a type.
When the option is clicked, it will change the typeface variable and fontsize variable.
```
<div class="div_class">
    <div class="dropdown">
        <button onclick="typefacefunction()" class="dropbtn" style="margin:33.33px;">Typeface</button>
        <div id="myDropdown1" class="dropdown-content">
            <a id="Arialbtn" onclick="typeface='Arial';">Arial</a>
            <a id="Albertina" onclick="typeface='Albertina';">Albertina</a>
            <a id="Calibribtn" onclick="typeface='Calibri';">Calibri</a>
            <a id="Impact" onclick="typeface='Impact';">Impact</a>
        </div>
    </div>
    <div class="dropdown">
        <button onclick="fontsizefunction()" class="dropbtn" style="margin:33.33px;">Fontsize</button>
        <div id="myDropdown2" class="dropdown-content">
            <a id="size_12" onclick="fontsize=12;">12</a>
            <a id="size_14" onclick="fontsize=14;">14</a>
            <a id="size_16" onclick="fontsize=16;">16</a>
            <a id="size_18" onclick="fontsize=18;">18</a>
            <a id="size_20" onclick="fontsize=20;">20</a>
            <a id="size_22" onclick="fontsize=22;">22</a>
            <a id="size_24" onclick="fontsize=24;">24</a>
            <a id="size_48" onclick="fontsize=48;">48</a>
            <a id="size_72" onclick="fontsize=72;">72</a>
            <a id="size_96" onclick="fontsize=96;">96</a>
        </div>
    </div>
</div>
```
When the typeface button or fontsize button is clicked, it will show or hide the menu.
If the mouse clicks on another place, the menu will hide.
```
function typefacefunction() 
{
    document.getElementById("myDropdown1").classList.toggle("show");
}

function fontsizefunction() 
{
    document.getElementById("myDropdown2").classList.toggle("show");
}
  
window.onclick = function(event) 
{
    if (!event.target.matches('.dropbtn')) 
    {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) 
        {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) 
                openDropdown.classList.remove('show');
        }
    }
}
```












