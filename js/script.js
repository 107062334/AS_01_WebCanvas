var mousecoord = 
{   
    pre_x : 0,
    pre_y : 0,
    cur_x : 0,
    cur_y : 0
};

var clicked = false;
var mode = "none";
var typing_text = false;
var typeface = "Arial";
var fontsize = 15;
var origin_x;
var origin_y;
var fors = "stroke";

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var undo_list=[];
var redo_list=[];
var img;

ctx.fillStyle = "black";
ctx.strokeStyle = "black";
ctx.lineJoin = "round";
ctx.lineCap = "round";
ctx.lineWidth = 10;

$(window).mousedown(update_state_down);
$(window).mouseup(update_state_up);
$("#canvas").mousedown(update_imagedata);
$("#canvas").mousemove(update_mousecoord);
$("#canvas").mousemove(draw);
$("#canvas").mousedown(textbox);
$(document).keypress(keyboardfunction);
$("button").mousedown(function(){clicked=false;})

$("#color").change(function(){ctx.fillStyle=this.value;ctx.strokeStyle=this.value;});
$("#pensize").change(function(){ctx.lineWidth=this.value;});

document.oncontextmenu = function(){
    return false;
}

function update_imagedata(event)
{
    if (clicked == false && event.button == 0 && mode!="text")
    {   
        redo_list=[];
        var imga = ctx.getImageData(0,0,1000,700);
        undo_list.push(imga);
        
    }
}

function update_state_down(event)
{
    if (clicked == false && event.button == 0)
    {   
        if (mode == "triangle" || mode == "circle" || mode == "rectangle")
        {
            img = ctx.getImageData(0,0,1000,700);
        }
        clicked = true;
        mousecoord.cur_x = event.pageX;
        mousecoord.cur_y = event.pageY;
        origin_x = mousecoord.cur_x;
        origin_y = mousecoord.cur_y;
    }
}

function update_state_up(event)
{
    if (clicked == true && event.button == 0)
    {  
        clicked = false;
    }
}

function update_mousecoord(event)
{   
    if (clicked == true)
    {
        mousecoord.pre_x = mousecoord.cur_x;
        mousecoord.pre_y = mousecoord.cur_y;
        mousecoord.cur_x = event.pageX
        mousecoord.cur_y = event.pageY;
    } 
}

function draw(event)
{   
    if (clicked == true && mode == "pen")
    {   
        ctx.beginPath();
        ctx.moveTo(mousecoord.pre_x-12,mousecoord.pre_y+10);
        ctx.lineTo(mousecoord.cur_x-12,mousecoord.cur_y+10);
        ctx.stroke();
    }
    else if (clicked == true && mode == "eraser")
    {   
        ctx.globalCompositeOperation = "destination-out";
        ctx.beginPath();
        ctx.moveTo(mousecoord.pre_x,mousecoord.pre_y);
        ctx.lineTo(mousecoord.cur_x,mousecoord.cur_y);
        ctx.stroke();
        ctx.globalCompositeOperation = "source-over";
    }
    else if (clicked == true && mode == "triangle")
    {   
        ctx.clearRect(0,0,1000,700);
        var length_x = origin_x - mousecoord.cur_x;
        var length_y = origin_y - mousecoord.cur_y;
        ctx.beginPath();
        ctx.moveTo(origin_x,origin_y);
        ctx.lineTo(origin_x-length_x,origin_y-length_y);
        ctx.lineTo(origin_x+length_x,origin_y-length_y);
        ctx.closePath();
        ctx.putImageData(img,0,0);
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
    else if (clicked == true && mode == "circle")
    {
        ctx.clearRect(0,0,1000,700);
        var radius = Math.sqrt((origin_x-mousecoord.cur_x)*(origin_x-mousecoord.cur_x)+(origin_y-mousecoord.cur_y)*(origin_y-mousecoord.cur_y))/2;
        ctx.beginPath();
        ctx.arc((origin_x+mousecoord.cur_x)/2,(origin_y+mousecoord.cur_y)/2,radius,0,2*Math.PI);
        ctx.putImageData(img,0,0);
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
    else if (clicked == true && mode == "rectangle")
    {
        ctx.clearRect(0,0,1000,700);
        var length_x = mousecoord.cur_x - origin_x;
        var length_y = mousecoord.cur_y - origin_y;
        ctx.beginPath();
        ctx.moveTo(origin_x,origin_y);
        ctx.lineTo(origin_x+length_x,origin_y);
        ctx.lineTo(origin_x+length_x,origin_y+length_y);
        ctx.lineTo(origin_x,origin_y+length_y);
        ctx.closePath();
        ctx.putImageData(img,0,0);
        
        if (fors == "stroke") ctx.stroke();
        else ctx.fill();
    }
}

function undo()
{   
    if (undo_list.length!=0)
    {   
        var cur_img = ctx.getImageData(0,0,1000,700);
        redo_list.push(cur_img);
        var prev_img = undo_list.pop();
        ctx.putImageData(prev_img,0,0);
    }  
}

function redo()
{   
    if (redo_list.length!=0)
    {   
        var cur_img = ctx.getImageData(0,0,1000,700);
        undo_list.push(cur_img);
        var next_img = redo_list.pop(); 
        ctx.putImageData(next_img,0,0);
    }    
}

function textbox(event)
{   
    if (typing_text == false && mode == "text")
    {
        typing_text = true;
        var text_input = document.createElement("input");
        text_input.id="text_input";
        var r = document.getElementById("left");
        var b = document.getElementById("body");
        text_input.setAttribute("type","text");
        text_input.setAttribute("autofocus","true");
        $(text_input).css("position","absolute");
        $(text_input).css("z-index","1");
        $(text_input).css("margin-top",event.clientY-19);
        $(text_input).css("margin-left",event.clientX-13);
        b.insertBefore(text_input,r);
    }    
}

function keyboardfunction(event)
{
    if (event.key == "Enter" && mode == "text" && typing_text == true)
    {   
        var imgb = ctx.getImageData(0,0,1000,700);
        undo_list.push(imgb);
        redo_list=[];
        var str = $("#text_input").val();
        var coordx = parseInt($("#text_input").css("margin-left"));
        var coordy = parseInt($("#text_input").css("margin-top"));
        $("#text_input").remove();
        typing_text = false;
        ctx.font = fontsize + "px " + typeface;
        ctx.fillText(str,coordx,coordy+11);
        
    }
}

function textonclick()
{   
    $(canvas).css("cursor","url(./icon/text_icon.png),auto");
    mode = "text";
}

function penonclick()
{   
    ctx.globalCompositeOperation = "source-over";
    $("#canvas").css("cursor","url('./icon/pen_cursor.png'),auto");
    mode="pen";
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
}

function eraseronclick()
{   
    
    $("#canvas").css("cursor","url('./icon/eraser_cursor.png'),auto");
    mode="eraser";
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
}

function reset()
{   

    mode = "none";
    fontsize = 15;
    typeface = "Arial";
    clicked = false;
    typing_text = false;
    undo_list=[];
    redo_list=[];
    var t = document.getElementById("text_input");
    if (t) $("#text_input").remove();
    var color = document.getElementById("color");
    color.value="black";
    ctx.lineWidth = 10;
    ctx.fillStyle = color.value;
    ctx.strokeStyle = color.value;
    ctx.clearRect(0,0,1000,700);
    $("#canvas").css("cursor","auto");
    var pensize = document.getElementById("pensize");
    pensize.value=10;
}

function typefacefunction() {
    document.getElementById("myDropdown1").classList.toggle("show");
}
function fontsizefunction() {
    document.getElementById("myDropdown2").classList.toggle("show");
}
  
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

function imageonchange(event)
{   
    var i = document.getElementById("image_input");
    var imgc = new Image();
    imgc.src = window.URL.createObjectURL(i.files[0]);
    var imgi = ctx.getImageData(0,0,1000,700);
    undo_list.push(imgi);
    redo_list=[];
    imgc.addEventListener("load",function()
    {   
        i.value = "";
        ctx.drawImage(imgc,0,0);
    })
}

function triangleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('./icon/triangle_cursor.png'),auto")
    mode = "triangle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/triangle_filled.png");
    else $("#fillorstrokeimg").attr("src","./icon/triangle_stroked.png"); 
}

function circleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('./icon/circle_cursor.png'),auto")
    mode = "circle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/circle_filled.png");
    else $("#fillorstrokeimg").attr("src","./icon/circle_stroke.png");
}

function rectangleonclick()
{   
    typing_text = false;
    var text_input = document.getElementById("text_input");
    if (text_input) $("#text_input").remove();
    $(canvas).css("cursor","url('./icon/rectangle_cursor.png'),auto")
    mode = "rectangle";
    if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/rectangle_filled.png");
    else $("#fillorstrokeimg").attr("src","./icon/rectangle_stroked.png");
}

function fillorstroke()
{
    fors = (fors == "stroke")?"fill":"stroke";
    if (mode == "triangle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/triangle_filled.png");
        else $("#fillorstrokeimg").attr("src","./icon/triangle_stroked.png"); 
    }
    else if (mode == "circle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/circle_filled.png");
        else $("#fillorstrokeimg").attr("src","./icon/circle_stroke.png");
    }
    else if (mode == "rectangle")
    {
        if (fors == "fill") $("#fillorstrokeimg").attr("src","./icon/rectangle_filled.png");
        else $("#fillorstrokeimg").attr("src","./icon/rectangle_stroked.png");
    }
}